package com.example.demo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class CurrencyDTO {

    @NotBlank(message = "Please provide the original currency name")
    @NotNull(message = "Please provide the original currency name")
    private String fromCurrency;
    @NotBlank(message = "Please provide the target currency name")
    @NotNull(message = "Please provide the target currency name")
    private String toCurrency;
    @NotNull(message = "Please provide the amount")
    private BigDecimal amount;

    public String getFromCurrency() {
        return fromCurrency;
    }

    public void setFromCurrency(String fromCurrency) {
        this.fromCurrency = fromCurrency;
    }

    public String getToCurrency() {
        return toCurrency;
    }

    public void setToCurrency(String toCurrency) {
        this.toCurrency = toCurrency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
