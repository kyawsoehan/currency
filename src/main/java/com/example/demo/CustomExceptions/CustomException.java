package com.example.demo.CustomExceptions;


import java.util.Date;

public class CustomException extends RuntimeException {

    private String message;
    private String details;
    private Date date;

    public CustomException(){}

    public CustomException(String message, String details){
        this.message = message;
        this.details = details;
        this.date = new Date();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
