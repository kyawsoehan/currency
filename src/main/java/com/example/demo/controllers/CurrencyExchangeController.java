package com.example.demo.controllers;

import com.example.demo.CurrencyDTO;
import com.example.demo.CustomExceptions.CustomException;
import com.example.demo.entities.ExchangeCurrency;
import com.example.demo.entities.ExchangeRecord;
import com.example.demo.repositories.ExchangeRecordRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;

@RestController
public class CurrencyExchangeController {

    @Autowired
    ExchangeRecordRepo exchangeRecordRepo;

    @GetMapping("/exchangerate")
    public String getExchangeRate(){
        return "";
    }

    @PostMapping("/exchange")
    public String exchange(@Valid @RequestBody CurrencyDTO currencyDTO){
        BigDecimal zeroDecimal = BigDecimal.valueOf(0);
        int result = currencyDTO.getAmount().compareTo(zeroDecimal);
        if (result == 0 || result == -1){
            throw new CustomException("Invalid amount!", "Please provide the valid amount");
        }else {

            ExchangeCurrency fromCurrency = new ExchangeCurrency();
            fromCurrency.setName(currencyDTO.getFromCurrency());
            ExchangeCurrency targetCurrency =  new ExchangeCurrency();
            targetCurrency.setName(currencyDTO.getToCurrency());


//            BigDecimal exchangedAmount = currencyDTO.getAmount().multiply(fromCurrency.getExchangeRate(targetCurrency));
            BigDecimal exchangedAmount = fromCurrency.getExchangedAmount(targetCurrency, currencyDTO.getAmount());
            ExchangeRecord record = new ExchangeRecord();
            record.setFromCurrency(fromCurrency.getName());
            record.setToCurrency(targetCurrency.getName());
            record.setAmount(currencyDTO.getAmount().toString());
            record.setExchangedAmount(exchangedAmount.toString());
            exchangeRecordRepo.save(record);
            return "Amount of " + currencyDTO.getAmount() + " " + currencyDTO.getFromCurrency() + " is exchanged to " + exchangedAmount
                    + " " + currencyDTO.getToCurrency();
        }
    }
}