package com.example.demo.entities;

import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
public class ExchangeCurrency extends Currency {

    @Override
    public BigDecimal getExchangeRate(Currency targetCurrency) {
        if (targetCurrency.getName().equals("USD")){
            return BigDecimal.valueOf(0.742164);
        }else if (targetCurrency.getName().equals("SGD")){
            return BigDecimal.valueOf(1.34782);
        }else {
            return null;
        }

    }

    @Override
    public BigDecimal getExchangedAmount(Currency targetCurrency, BigDecimal amount) {
        return targetCurrency.getExchangeRate(targetCurrency).multiply(amount);
    }
}