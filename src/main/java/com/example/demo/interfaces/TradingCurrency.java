package com.example.demo.interfaces;

import com.example.demo.entities.Currency;

import java.math.BigDecimal;

public interface TradingCurrency {
    BigDecimal getExchangeRate(Currency targetCurrency);
    BigDecimal getExchangedAmount(Currency targetCurrency, BigDecimal amount);
}


