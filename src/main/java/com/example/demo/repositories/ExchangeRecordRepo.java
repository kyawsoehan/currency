package com.example.demo.repositories;

import com.example.demo.entities.ExchangeRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExchangeRecordRepo extends JpaRepository<ExchangeRecord, Long> {
}
